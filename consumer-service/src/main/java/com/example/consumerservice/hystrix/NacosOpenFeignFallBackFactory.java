package com.example.consumerservice.hystrix;

import com.example.consumerservice.entity.User;
import com.example.consumerservice.result.Result;
import com.example.consumerservice.service.NacosOpenfeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class NacosOpenFeignFallBackFactory implements FallbackFactory<NacosOpenfeignClient> {
    @Override
    public NacosOpenfeignClient create(final Throwable cause) {  // 这里就是统一处理接口限流

        return new NacosOpenfeignClient(){


            @Override
            public Result<String> test() {
                return null;
            }

            @Override
            public Result getUser(Long id) {
                return getResult();
            }

            @Override
            public Result insertUser(User user) {
                return getResult();
            }

            @Override
            public Result updateUser(User user) {
                return getResult();
            }

            @Override
            public Result deleteUser(Long id) {
                return getResult();
            }

            private Result getResult() {
                log.info(cause.getMessage());
                System.out.println("异常信息 ->:" + cause.getMessage());
                cause.printStackTrace();
                if (cause.getMessage().contains("Load balancer does not contain an instance")){ //这个是找不到实例
                    return Result.fail("服务器开小差了，请稍后再试",cause.getMessage());
                }
                return Result.fail("未知异常 -> :",cause.getMessage());
            }
        };

    }
}
