package com.example.consumerservice.hystrix;

import com.example.consumerservice.entity.User;
import com.example.consumerservice.result.Result;
import com.example.consumerservice.service.NacosOpenfeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class NacosOpenFeignFallBack implements NacosOpenfeignClient {

//    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Result<String> test() {
        return null;
    }

    // 这种方式无法获取异常，只能自定义异常  调用异常就返回一个我们自己定义的信息回去
    @Override
    public Result getUser(Long id) {
        log.info("获取用户信息失败");
        return Result.fail("获取用户信息失败");
    }

    @Override
    public Result insertUser(User user) {
        return null;
    }

    @Override
    public Result updateUser(User user) {
        return null;
    }

    @Override
    public Result deleteUser(Long id) {
        return null;
    }
}
