package com.example.consumerservice.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class OpenfeignConfig {

    // openfeign 的日志  通过config形式进行设置   还需要在openfeignClient中进行引入
    @Bean
    Logger.Level feignLoggerLevel(){
        return Logger.Level.BASIC;
    }
}
