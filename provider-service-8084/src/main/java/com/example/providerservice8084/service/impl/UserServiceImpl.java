package com.example.providerservice8084.service.impl;


import com.example.providerservice8084.entity.User;
import com.example.providerservice8084.mapper.UserMapper;
import com.example.providerservice8084.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Value("${user.name}")
    private String name;

    @Autowired
    private Environment environment;

    @Resource
    private UserMapper userMapper;

    @Override
    public User getUser(Long id) {
        User user = userMapper.getUser(id);
       /* int a =new Random().nextInt(10);
        if (a>8){
            throw new BizException("这是一个测试错误");
        }else if (a>4){
            int b = 10/0;
        }*/

        /*测试nacos动态刷新配置*/
        /*while(true) {
            String userName = environment.getProperty("user.name");
            System.out.println("environment -- >" + userName);
            System.out.println("------------");
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }*/
        if (user == null) {
            user = new User();
        }
        String property = environment.getProperty("server.port");
        user.setPort(property);
        return user;
    }

    @Override
    public Integer insertUser(User user) {
        int insert = userMapper.insert(user);
        return insert;
    }

    @Override
    public Integer updateUser(User user) {
        /*LambdaUpdateWrapper<User> wrapper = Wrappers.lambdaUpdate();
        wrapper.setEntity(user);
        wrapper.set(User::getName,user.getName());
        wrapper.set(User::getAge,user.getAge());*/
        // 修改肯定是知道id呢 所以可以updateById
        userMapper.updateById(user);
        return null;
    }

    @Override
    public Integer deleteUser(Long id) {
        return userMapper.deleteById(id);
    }
}
