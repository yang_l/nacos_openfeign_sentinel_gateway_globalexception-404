package com.example.providerservice8084.service;


import com.example.providerservice8084.entity.User;

public interface UserService {
    User getUser(Long id);

    Integer insertUser(User user);

    Integer updateUser(User user);

    Integer deleteUser(Long id);
}
