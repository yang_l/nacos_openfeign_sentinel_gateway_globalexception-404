package com.example.providerservice8084.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;

import com.example.providerservice8084.entity.User;
import com.example.providerservice8084.result.Result;
import com.example.providerservice8084.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("user")
@SentinelResource
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("getUser")
    public Result<User> getUser(Long id) {
        User user = userService.getUser(id);
        return Result.ok(user);
    }

    @PostMapping("insertUser")
    public Result<Integer> insertUser(@RequestBody User user) {
        Integer integer = userService.insertUser(user);
        return Result.ok(integer);
    }

    @PutMapping("putUser")
    public Result<Integer> updateUser(@RequestBody User user) {
        Integer integer = userService.updateUser(user);
        return Result.ok(integer);
    }

    @DeleteMapping("deleteUser")
    public Result<Integer> deleteUser(Long id) {
        Integer integer = userService.deleteUser(id);
        return Result.ok(integer);
    }
}
