package com.example.providerservice8084.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.providerservice8084.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    User getUser(@Param("id") Long id);
}
