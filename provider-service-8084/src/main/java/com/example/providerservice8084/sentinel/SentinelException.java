package com.example.providerservice8084.sentinel;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.BlockExceptionHandler;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowException;
import com.alibaba.csp.sentinel.slots.system.SystemBlockException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class SentinelException implements BlockExceptionHandler {
    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, BlockException e) throws Exception {
        log.info("sentinel exception ....");
        // 官方处理方式
        /*httpServletResponse.setStatus(500);
        httpServletResponse.setCharacterEncoding("utf-8");
        httpServletResponse.setHeader("Content-Type", "application/json;charset=utf-8");
        httpServletResponse.setContentType("application/json;charset=utf-8");

        PrintWriter out = httpServletResponse.getWriter();
        if (e instanceof FlowException) {
            httpServletResponse.setStatus(1001);
            out.print("接口限流"); // 出现中文乱码问题  因为HttpServletResponse 需要指定字符集
        } else if (e instanceof DegradeException) {
            httpServletResponse.setStatus(1002);
            out.print("服务降级");
        } else if (e instanceof ParamFlowException) {
            httpServletResponse.setStatus(1003);
            out.print("热点参数限流");
        } else if (e instanceof SystemBlockException) {
            httpServletResponse.setStatus(1004);
            out.print("系统规则负载或不满足要求...");
        } else if (e instanceof AuthorityException) {
            httpServletResponse.setStatus(1005);
            out.print("没有权限");
        }
        out.flush();
        out.close();*/
        //自定义处理方式
        SentinelErrorMsg sentinelErrorMsg = new SentinelErrorMsg();
        if (e instanceof FlowException) {
            sentinelErrorMsg.setMsg("接口限流了");
            sentinelErrorMsg.setStatus(101);
        } else if (e instanceof DegradeException) {
            sentinelErrorMsg.setMsg("服务降级了");
            sentinelErrorMsg.setStatus(102);
        } else if (e instanceof ParamFlowException) {
            sentinelErrorMsg.setMsg("热点参数限流了");
            sentinelErrorMsg.setStatus(103);
        } else if (e instanceof SystemBlockException) {
            sentinelErrorMsg.setMsg("系统规则（负载/...不满足要求）");
            sentinelErrorMsg.setStatus(104);
        } else if (e instanceof AuthorityException) {
            sentinelErrorMsg.setMsg("授权规则不通过");
            sentinelErrorMsg.setStatus(105);
        }
        // http状态码
        httpServletResponse.setStatus(500);
        httpServletResponse.setCharacterEncoding("utf-8");
        httpServletResponse.setHeader("Content-Type", "application/json;charset=utf-8");
        httpServletResponse.setContentType("application/json;charset=utf-8");
        // spring mvc自带的json操作工具，叫jackson
        new ObjectMapper()
                .writeValue(
                        httpServletResponse.getWriter(),
                        sentinelErrorMsg
                );

        // 这个是跳转到页面  需要自己也一个index.jsp 的页面  前后端分离跳转会失败
        /*httpServletRequest.getRequestDispatcher( "/index.jsp").forward(httpServletRequest, httpServletResponse);*/

        //   前后端分离可以试试重定向  这个没有验证 不知道可行不
        /*httpServletResponse.sendRedirect("www.baidu.com");*/
    }

    // 内部类 需要为static
    /*@Data
    static class SentinelErrorMsg {
        private Integer status;
        private String msg;
    }*/
}
