package com.example.providerservice8084.sentinel;

import lombok.Data;

@Data
public class SentinelErrorMsg {

    private Integer status;

    private String msg;
}
