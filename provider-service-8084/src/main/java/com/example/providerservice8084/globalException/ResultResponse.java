package com.example.providerservice8084.globalException;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * @description: 自定义数据传输
 * @author:
 * @date:
 */
@Data
public class ResultResponse {
    /**
     * 响应代码
     */
    private String code;

    /**
     * 响应消息
     */
    private String message;

    /**
     * 响应结果
     */
    private Object result;


    public ResultResponse() {
    }

    public ResultResponse(BaseErrorInfoInterface errorInfo) {
        this.code = errorInfo.getResultCode();
        this.message = errorInfo.getResultMsg();
    }

    /**
     * 成功
     *
     * @return
     */
    public static ResultResponse success() {
        return success(null);
    }

    /**
     * 成功
     *
     * @param data
     * @return
     */
    public static ResultResponse success(Object data) {
        ResultResponse rb = new ResultResponse();
        rb.setCode(ExceptionEnum.SUCCESS.getResultCode());
        rb.setMessage(ExceptionEnum.SUCCESS.getResultMsg());
        rb.setResult(data);
        return rb;
    }

    /**
     * 失败
     */
    public static ResultResponse error(BaseErrorInfoInterface errorInfo) {
        ResultResponse rb = new ResultResponse();
        rb.setCode(errorInfo.getResultCode());
        rb.setMessage(errorInfo.getResultMsg());
        rb.setResult(null);
        return rb;
    }

    /**
     * 失败
     */
    public static ResultResponse error(String code, String message) {
        ResultResponse rb = new ResultResponse();
        rb.setCode(code);
        rb.setMessage(message);
        rb.setResult(null);
        return rb;
    }

    /**
     * 失败
     */
    public static ResultResponse error(ExceptionEnum exceptionEnum) {
        ResultResponse rb = new ResultResponse();
        rb.setCode(exceptionEnum.getResultCode());
        rb.setMessage(exceptionEnum.getResultMsg());
        rb.setResult(null);
        return rb;
    }


    /**
     * 失败
     */
    public static ResultResponse error(String code, String message, Object result) {
        ResultResponse rb = new ResultResponse();
        rb.setCode(code);
        rb.setMessage(message);
        rb.setResult(result);
        return rb;
    }

    /**
     * 失败
     */
    public static ResultResponse error(ExceptionEnum exceptionEnum, Object result) {
        ResultResponse rb = new ResultResponse();
        rb.setCode(exceptionEnum.getResultCode());
        rb.setMessage(exceptionEnum.getResultMsg());
        rb.setResult(result);
        return rb;
    }

    /**
     * 失败
     */
    public static ResultResponse error(String message) {
        ResultResponse rb = new ResultResponse();
        rb.setCode("-1");
        rb.setMessage(message);
        rb.setResult(null);
        return rb;
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }

}