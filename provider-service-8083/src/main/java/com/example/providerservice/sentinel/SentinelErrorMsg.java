package com.example.providerservice.sentinel;

import lombok.Data;

@Data
public class SentinelErrorMsg {

    private Integer status;

    private String msg;
}
