package com.example.providerservice.globalException;

import com.alibaba.csp.sentinel.slots.block.authority.AuthorityException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowException;
import com.alibaba.csp.sentinel.slots.system.SystemBlockException;
import com.alibaba.fastjson.JSONObject;
import com.example.providerservice.exception.MyException;
import feign.FeignException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @description: 自定义异常处理
 * @author:
 * @date:
 * @version:
 */
@ControllerAdvice
@Slf4j
@Data
/*实现 ResponseBodyAdvice<Object>接口 是为了对结果进行包装，如果不需要可以不用实现该接口*/
/*实现 RequestBodyAdvice<Object>接口 是为了对请求参数进行包装，如果不需要可以不用实现该接口*/

public class GlobalExceptionHandler implements ResponseBodyAdvice<Object>, RequestBodyAdvice {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 处理自定义的业务异常
     *
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value = BizException.class)
    @ResponseBody
    public ResultResponse bizExceptionHandler(HttpServletRequest req, BizException e) {
        logger.error("发生业务异常！原因是：{}", e.getErrorMsg());
        return ResultResponse.error(e.getErrorCode(), e.getErrorMsg());
    }

    /*@ExceptionHandler(value = MyException.class)
    @ResponseBody
    public ResultResponse bizExceptionHandler(HttpServletRequest req, MyException e) {
        logger.error("发生业务异常！原因是：{}", e.getMessage());
        return ResultResponse.error(e.getMessage());
    }*/

    /**
     * 处理空指针的异常
     *
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value = NullPointerException.class)
    @ResponseBody
    public ResultResponse exceptionHandler(HttpServletRequest req, NullPointerException e) {
        logger.error("空指针异常！原因是:", e);
        return ResultResponse.error(ExceptionEnum.NULLPOINTER_EXCEPTION);
    }

    /**
     * 处理类型转换异常
     *
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value = NumberFormatException.class)
    @ResponseBody
    public ResultResponse exceptionHandler(HttpServletRequest req, NumberFormatException e) {
        logger.error("类型转换异常！原因是:", e);
        return ResultResponse.error(ExceptionEnum.PARAMS_NOT_CONVERT);
    }

    @ExceptionHandler(value = ArithmeticException.class)
    @ResponseBody
    public ResultResponse exceptionHandler(HttpServletRequest req, ArithmeticException e) {
        logger.error("算数异常！原因是:", e);
        return ResultResponse.error(ExceptionEnum.ARITHMETIC_EXCEPTION, e.getMessage());
    }

    /**
     * 处理空参数不合法
     *
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value = IllegalStateException.class)
    @ResponseBody
    public ResultResponse exceptionHandler(HttpServletRequest req, IllegalStateException e) {
        logger.error("参数不合法！原因是:", e);
        return ResultResponse.error(ExceptionEnum.ILLEGAL_STATE);
    }

    /**
     * 处理sentinel抛出的异常
     */
    @ExceptionHandler(value = FlowException.class)
    @ResponseBody
    public ResultResponse exceptionHandler(HttpServletRequest req, FlowException e) {
        logger.error("接口限流！原因是:", e);
        return ResultResponse.error(ExceptionEnum.FLOW_EXCEPTION);
    }

    @ExceptionHandler(value = DegradeException.class)
    @ResponseBody
    public ResultResponse exceptionHandler(HttpServletRequest req, DegradeException e) {
        logger.error("流量降级！原因是:", e);
        return ResultResponse.error(ExceptionEnum.DEGRADE_EXCEPTION);
    }

    @ExceptionHandler(value = ParamFlowException.class)
    @ResponseBody
    public ResultResponse exceptionHandler(HttpServletRequest req, ParamFlowException e) {
        logger.error("参数限流！原因是:", e);
        return ResultResponse.error(ExceptionEnum.PARAM_FLOW_EXCEPTION);
    }

    @ExceptionHandler(value = SystemBlockException.class)
    @ResponseBody
    public ResultResponse exceptionHandler(HttpServletRequest req, SystemBlockException e) {
        logger.error("系统保护！原因是:", e);
        return ResultResponse.error(ExceptionEnum.SYSTEM_BLOCK_EXCEPTION);
    }

    @ExceptionHandler(value = AuthorityException.class)
    @ResponseBody
    public ResultResponse exceptionHandler(HttpServletRequest req, AuthorityException e) {
        logger.error("没有访问权限！原因是:", e);
        return ResultResponse.error(ExceptionEnum.AUTHORITY_EXCEPTION);
    }


    /**
     * 处理其他异常
     *
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResultResponse exceptionHandler(HttpServletRequest req, Exception e) {
        logger.error("未知异常！原因是:", e);
        return ResultResponse.error(ExceptionEnum.INTERNAL_SERVER_ERROR);
    }

    // 下面是对返回结果 和 请求参数 进行拦截
    /*这个方法也是对实现的接口中的方法  进行重写*/
    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        /* 如需使用这个功能  需要将下面这个return true*/
        /*return true;*/
        return false;
    }

    /*这里就是对结果进行处理*/
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        /*body 就是我们返回的数据   我们可以在这里再次对这个数据进行封装 */
        /*举个例子  如果是String类型我们就 转成JSON格式的 */
        if (body instanceof String) {
            Object parse = JSONObject.parse((String) body);
        }
        return body;
    }

    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        /* 如需使用这个功能  需要将下面这个return true*/
        /*return true;*/
        return false;
    }

    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
        return null;
    }

    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return null;
    }

    @Override
    public Object handleEmptyBody(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return null;
    }
}
