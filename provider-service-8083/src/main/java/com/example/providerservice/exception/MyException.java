package com.example.providerservice.exception;

public class MyException extends RuntimeException {

    private Integer errorCode;

    //无参构造方法
    public MyException() {
        super();
    }

    public MyException(String message) {
        super(message);
    }

    public MyException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyException(Throwable cause) {
        super(cause);
    }

    public MyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public MyException(Integer errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public MyException(Integer errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public MyException(Integer errorCode, Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
    }

    public MyException(Integer errorCode, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }

//    //有参的构造方法
//    public MyException(String message){
//        super(message);
//
//    }
//
//    // 用指定的详细信息和原因构造一个新的异常
//    public MyException(String message, Throwable cause){
//        super(message,cause);
//    }
//
//    //用指定原因构造一个新的异常
//    public MyException(Throwable cause) {
//        super(cause);
//    }

}
