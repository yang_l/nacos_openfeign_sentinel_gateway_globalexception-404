package com.example.providerservice.service;


import com.example.providerservice.entity.User;

public interface UserService {
    User getUser(Long id);

    Integer insertUser(User user);

    Integer updateUser(User user);

    Integer deleteUser(Long id);
}
