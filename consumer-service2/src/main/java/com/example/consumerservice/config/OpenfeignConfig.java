package com.example.consumerservice.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author yang_li
 */
@Configuration
public class OpenfeignConfig {

    /**
     * openfiegn 日志几倍
     * @return
     */
    @Bean
    Logger.Level feignLoggerLevel(){
        return Logger.Level.BASIC;
    }
}
