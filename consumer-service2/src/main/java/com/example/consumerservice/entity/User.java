package com.example.consumerservice.entity;


import lombok.Data;

@Data
public class User {

    private Long id;

    private String name;

    private Integer age;

    // 服务提供者的端口
    private String port;

    // 服务消费者的端口
    private String consumerPort;
}
