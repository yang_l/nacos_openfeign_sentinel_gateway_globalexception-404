package com.example.consumerservice.controller;

import com.example.consumerservice.entity.User;
import com.example.consumerservice.result.Result;
import com.example.consumerservice.service.NacosOpenfeignClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

/**
 * @author yang_li
 */
@RestController
@RequestMapping("test")
public class NacosOpenfeignController {

    @Autowired
    private NacosOpenfeignClient nacosOpenfeignClient;

    @Autowired
    private Environment environment;

    // 对此方法开启熔断机制  如果加了fallbackMethod 那么就不会进入fallbackFactory被统一处理进行兜底  会用自己配置的方法进行兜底
    // @HystrixCommand(fallbackMethod = "userCircuitBreaker_fallback",commandProperties = {
    @HystrixCommand(commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled",value ="true"),//开启服务熔断降级
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "10"),// 熔断出发的最小个数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),//时间范围
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "20") // 失败率达到多少之后熔断
                    })
    @GetMapping("getUserTest")
    public Result<User> getUser(Long id) {

        Result<User> user = nacosOpenfeignClient.getUser(id);
        String property = environment.getProperty("server.port");
        User data = user.getData();
        data.setConsumerPort(property);
        user.setData(data);
        return user;
    }

    public Result<User> userCircuitBreaker_fallback(Long id){
        return Result.fail("访问过于频繁 服务熔断 请稍后再试");
    }

    @PostMapping("insertUserTest")
    Result<Integer> insertUser(@RequestBody User user ){
        return nacosOpenfeignClient.insertUser(user);
    }

    @PutMapping("putUserTest")
    Result<Integer> updateUser(@RequestBody User user){
        return nacosOpenfeignClient.updateUser(user);
    }

    @DeleteMapping("deleteUserTest")
    Result<Integer> deleteUser(@RequestParam("id") Long id){
        return nacosOpenfeignClient.deleteUser(id);
    }
}
