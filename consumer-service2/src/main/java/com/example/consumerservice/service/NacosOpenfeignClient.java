package com.example.consumerservice.service;

import com.example.consumerservice.config.OpenfeignConfig;
import com.example.consumerservice.entity.User;
import com.example.consumerservice.hystrix.NacosOpenFeignFallBack;
import com.example.consumerservice.hystrix.NacosOpenFeignFallBackFactory;
import com.example.consumerservice.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "provider-service",fallbackFactory = NacosOpenFeignFallBackFactory.class,configuration = OpenfeignConfig.class) // 提供者的服务名称
@Component
public interface NacosOpenfeignClient {

    @GetMapping("user/getUser")
    /*在正常书写的controller中 get请求是不需要使用 @RequestParam("xx")来解析请求头中的参数的  */
    /*但是在使用openfeign进行调用的时候 需要加上@RequestParam("xx")来解析请求头中的参数 */
    Result<User> getUser(@RequestParam("id") Long id);

    @PostMapping("user/insertUser")
    Result<Integer> insertUser(@RequestBody User user );

    @PutMapping("user/putUser")
    Result<Integer>  updateUser(@RequestBody User user);

    @DeleteMapping("user/deleteUser")
    Result<Integer>  deleteUser(@RequestParam("id") Long id);
}
