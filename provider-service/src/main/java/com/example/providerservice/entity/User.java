package com.example.providerservice.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.checkerframework.checker.units.qual.Length;

@Data
@TableName("user")
public class User {

    @TableId(value = "id",type = IdType.AUTO)
    @Length()
    private Long id;

    @TableField("name")
    private String name;

    @TableField("age")
    private Integer age;

    private String port;
}
