package com.example.gatewayservice.config;

import com.alibaba.nacos.common.utils.StringUtils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

@Component // 只能使用@Component,不能使用 @Configuration
@Slf4j
public class TokenRoutePredicateFactory extends AbstractRoutePredicateFactory<TokenConfig> {

    // 手写断言 predicate  这样我们就可以在 predicates中使用我们自己定义的断言了

    public TokenRoutePredicateFactory() {
        super(TokenConfig.class);
    }

    @Override // 如果不实现这个方法 无法识别到我们定义的谓词
    public List<String> shortcutFieldOrder() { // 我们定义一个叫Token的谓词 这个就是谓词的字段  就是需要他带上token这个字段
        return Collections.singletonList("token");
    }

    @Override // 判断查询参数中有没有token的字段 且token字段的值是否等于配置中的值
    public Predicate<ServerWebExchange> apply(TokenConfig config) {
        return exchange->{
            MultiValueMap<String, String> queryParams = exchange.getRequest()
                    .getQueryParams(); // 获取所有的参数的键值对 搞成一个map

            boolean flag = false;

            for (Map.Entry<String, List<String>> entry : queryParams.entrySet()){
                StringBuilder stringBuilder = new StringBuilder();
                for (String s : entry.getValue()){
                    stringBuilder.append(s).append("+");
                    if (StringUtils.equalsIgnoreCase(s,config.getToken())){
                        flag = true;
                    }
                }
                log.info(entry.getKey()+" -> {}",stringBuilder.substring(0,stringBuilder.length()-1));
            }
            return flag;
        };
    }


}
