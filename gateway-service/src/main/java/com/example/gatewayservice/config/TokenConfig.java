package com.example.gatewayservice.config;

import lombok.Data;

@Data
public class TokenConfig {

    private String token;

}
