package com.example.gatewayservice.sentinelException;

import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.BlockRequestHandler;
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.GatewayCallbackManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;

@Component
public class SentinelException {

//    @Override
//    public Mono<ServerResponse> handleRequest(ServerWebExchange serverWebExchange, Throwable throwable) {
//        return ServerResponse.status(HttpStatus.BAD_GATEWAY)
//                .contentType(MediaType.APPLICATION_JSON)
//                .body(BodyInserters.fromValue("服务器太热了,罢工了,请稍后再试..."));
//    }
//
//    @PostConstruct
//    public void init(){
//        GatewayCallbackManager.setBlockHandler(new SentinelException());
//    }
}
