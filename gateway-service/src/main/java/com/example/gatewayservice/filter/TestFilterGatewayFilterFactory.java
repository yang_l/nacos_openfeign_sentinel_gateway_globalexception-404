package com.example.gatewayservice.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractNameValueGatewayFilterFactory;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TestFilterGatewayFilterFactory extends AbstractNameValueGatewayFilterFactory {

    // 使用 TestFilter 这个名字  可以在配置文件中定义路由的 filter 、只针对某一个route

    @Override  // 这里是具体实现...
    public GatewayFilter apply(NameValueConfig config) {
        return null;
    }
}
