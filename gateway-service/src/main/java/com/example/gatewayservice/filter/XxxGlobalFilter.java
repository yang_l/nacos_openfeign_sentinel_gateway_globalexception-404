package com.example.gatewayservice.filter;


import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

//@Component
@Slf4j  // 自定义全局filter  配置字段为Xxx  针对所有的 route
public class XxxGlobalFilter implements org.springframework.cloud.gateway.filter.GlobalFilter, Ordered {

    @Override  // 具体实现
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        return null;
    }

    @Override // 数字越小越先执行
    public int getOrder() {
        return 0;
    }
}
